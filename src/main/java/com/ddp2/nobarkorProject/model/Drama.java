package com.ddp2.nobarkorProject.model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class Drama {
    private static ArrayList<Drama> daftarDrama = new ArrayList<Drama>();
    private String nama;
    private String genre;
    private String tahun;
    private double rating;
    private String pemain;
    private String sinopsis;
    private String review;
    private String gambar;
    private static BufferedReader br;

    // Constructor untuk Drama
    public Drama(String nama, String genre, String tahun, double rating, String pemain, String sinopsis, String review, String gambar) {
        this.nama = nama;
        this.genre = genre;
        this.tahun = tahun;
        this.rating = rating;
        this.pemain = pemain;
        this.sinopsis = sinopsis;
        this.review = review;
        this.gambar = gambar;
    }

    public Drama() {}

    // Getter untuk nama
    public String getNama() {
        return this.nama;
    }

    // Getter untuk genre
    public String getGenre() {
        return this.genre;
    }

    // Getter untuk tahun
    public String getTahun() {
        return this.tahun;
    }

    // Getter untuk rating
    public double getRating() {
        return this.rating;
    }
    
    // Getter untuk pemain
    public String getPemain() {
        return pemain;
    }

    // Getter untuk sinopsis
    public String getSinopsis() {
        return sinopsis;
    }

    // Getter untuk review
    public String getReview() {
        return review;
    }

    // Getter untuk gambar
    public String getGambar() {
        return gambar;
    }

    // Getter untuk list getDaftarDrama
    public static ArrayList<Drama> getDaftarDrama() {
        return daftarDrama;
    }

    // Menambahkan object drama baru ke dalam list
    public static void tambahDrama(Drama drama) {
        getDaftarDrama().add(drama);
    }

    // Mengambil data dari file Data.txt
    public static void input() {
        try {
            File f = new File("Data.txt");
            br = new BufferedReader(new FileReader(f));
            String input = br.readLine();
            while (input != null) {
                String[] str = input.split(";");
                    Drama data = new Drama(str[0], str[3], str[1], Double.parseDouble(str[2]), str[4], str[5], str[6], str[7]);
                    tambahDrama(data);
                    input = br.readLine();
                } 
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
