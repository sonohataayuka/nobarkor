package com.ddp2.nobarkorProject.model;

public class InputOutput {
    private String inputJudul;
    private String tahunDrama;
    private String namaPemain;
    private double ratingDrama;
    private String genreDrama;

    /**
     * @param genreDrama the genreDrama to set
     */
    public void setGenreDrama(String genreDrama) {
        this.genreDrama = genreDrama;
    }

    /**
     * @return the genreDrama
     */
    public String getGenreDrama() {
        return genreDrama;
    }

    /**
     * @param inputJudul the inputJudul to set
     */
    public void setInputJudul(String inputJudul) {
        this.inputJudul = inputJudul;
    }

    /**
     * @return the inputJudul
     */
    public String getInputJudul() {
        return inputJudul;
    }

    /**
     * @param namaPemain the namaPemain to set
     */
    public void setNamaPemain(String namaPemain) {
        this.namaPemain = namaPemain;
    }

    /**
     * @return the namaPemain
     */
    public String getNamaPemain() {
        return namaPemain;
    }

    /**
     * @param ratingDrama the ratingDrama to set
     */
    public void setRatingDrama(double ratingDrama) {
        this.ratingDrama = ratingDrama;
    }

    /**
     * @return the ratingDrama
     */
    public double getRatingDrama() {
        return ratingDrama;
    }

    /**
     * @param tahunDrama the tahunDrama to set
     */
    public void setTahunDrama(String tahunDrama) {
        this.tahunDrama = tahunDrama;
    }

    /**
     * @return the tahunDrama
     */
    public String getTahunDrama() {
        return tahunDrama;
    }
    
}