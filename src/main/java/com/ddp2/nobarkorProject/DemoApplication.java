package com.ddp2.nobarkorProject;

import com.ddp2.nobarkorProject.model.Drama;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		Drama.input();
		SpringApplication.run(DemoApplication.class, args);
	}

}
