package com.ddp2.nobarkorProject;

import java.util.ArrayList;
import com.ddp2.nobarkorProject.model.Drama;
import com.ddp2.nobarkorProject.model.InputOutput;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
public class MainController {
    
    // Mapping untuk halaman pertama (Home)
    @GetMapping("/index.html")
    public String index(Model model) {
        model.addAttribute("dramaDicari", Drama.getDaftarDrama());
        return "index";
    }

    // Mapping untuk menampilkan semua data drama
    @GetMapping("/semuaData")
    public String dataSemua(@ModelAttribute InputOutput inputOutput, Model model) {
        model.addAttribute("dramaDicari", Drama.getDaftarDrama());
        return "dataDrama";
    }
    
    // Mapping untuk fitur mencari berdasarkan genre
    @GetMapping("/genre.html")
    public String berdasarkanGenre(Model model) {
        model.addAttribute("inputOutput", new InputOutput());
        return "genre";
    }

    // Mapping untuk menampilkan hasil data pencarian berdasarkan genre
    @PostMapping("/genre.html")
    public String dataGenre(@ModelAttribute InputOutput inputOutput, Model model) {
        ArrayList<Drama> listBaru = new ArrayList<>();
        for (Drama drama : Drama.getDaftarDrama()){
            if (drama.getGenre().toLowerCase().replaceAll("[\\W]", "").contains(inputOutput.getGenreDrama().toLowerCase().replaceAll("[\\W]", "").replaceAll("\\d", ""))) {
                    listBaru.add(drama);
                }
        }

        model.addAttribute("dramaDicari", listBaru);
        return "dataDrama";
    }

    // Mapping untuk fitur mencari berdasarkan tahun
    @GetMapping("/tahun.html")
    public String berdasarkanTahun(Model model) {
        model.addAttribute("inputOutput", new InputOutput());
        return "tahun";
    }

    // Mapping untuk menampilkan hasil data pencarian berdasarkan tahun
    @PostMapping("/tahun.html")
    public String dataTahun(@ModelAttribute InputOutput inputOutput, Model model) {
        ArrayList<Drama> listBaru = new ArrayList<>();
        for (Drama drama : Drama.getDaftarDrama()){
            if((drama.getTahun()).replaceAll("[\\W]", "").contains(inputOutput.getTahunDrama().replaceAll("[\\W]", "").replaceAll("[\\D]", ""))) {
                    listBaru.add(drama);
                }
        }

        model.addAttribute("dramaDicari", listBaru);
        return "dataDrama";
    }
    
    // Mapping untuk fitur mencari berdasarkan nama pemain
    @GetMapping("/pemain.html")
    public String berdasarkanPemain(Model model) {
        model.addAttribute("inputOutput", new InputOutput());
        return "pemain";
    }

    // Mapping untuk menampilkan hasil data pencarian berdasarkan pemain
    @PostMapping("/pemain.html")
    public String dataPemain(@ModelAttribute InputOutput inputOutput, Model model) {
        ArrayList<Drama> listBaru = new ArrayList<>();
        for (Drama drama : Drama.getDaftarDrama()){
            if ((drama.getPemain().toLowerCase()).replaceAll("[\\W]", "").contains((inputOutput.getNamaPemain().toLowerCase()).replaceAll("[\\W]", ""))) {
                    listBaru.add(drama);
                }
        }

        model.addAttribute("dramaDicari", listBaru);
        return "dataDrama";
    }

    // Mapping untuk fitur mencari berdasarkan judul drama
    @GetMapping("/review.html")
    public String review(Model model) {
        model.addAttribute("inputOutput", new InputOutput());
        return "review";
    }

    // Mapping untuk menampilkan hasil data pencarian berdasarkan judul
    @PostMapping("/review.html")
    public String dataDrama(@ModelAttribute InputOutput inputOutput, Model model) {
        ArrayList<Drama> listBaru = new ArrayList<>();
        for (Drama drama : Drama.getDaftarDrama()){
        if (drama.getNama().toLowerCase().contains(inputOutput.getInputJudul().toLowerCase())) {
                listBaru.add(drama);
            }
        }

        model.addAttribute("dramaDicari", listBaru);
        return "dataDrama";
    }

    // Mapping untuk fitur menampilan data dari satu drama yang dipilih
    @GetMapping("/drama.html")
    public String dramaSolo(@RequestParam(defaultValue = "") String name, Model model) {
        Drama dramaNih = null;
        for (Drama drama : Drama.getDaftarDrama()) {
            if(name.toLowerCase().contains(drama.getNama().toLowerCase())){
                dramaNih = drama; 
            }
        }
        model.addAttribute("dramaDicari", dramaNih);
        return "drama";
    }
}